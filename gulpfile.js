'use strict';

const gulp = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concatCss = require('gulp-concat-css');
const imageMin = require('gulp-imagemin');
const concat = require('gulp-concat');
const pump = require('pump');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const gulpSequence = require('gulp-sequence');


// Clean task
gulp.task('clean-dist', function() {
  return gulp.src('./dist', {read: false})
    .pipe(clean());
});

// CSS task
gulp.task('sass', function() {
  return gulp.src('./src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concatCss('style.css'))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./dist/css'));
});

// Images task
gulp.task('image-min', function() {
  return gulp.src('./src/img/*')
    .pipe(imageMin())
    .pipe(gulp.dest('./dist/img'));
});

// JS tasks
gulp.task('concat-js', function() {
  return gulp.src(['./src/js/**/*.js', '!./src/js/**/all.js'])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./src/js'));
});

gulp.task('compress-js', function (cb) {
  pump([
      gulp.src('./src/js/all.js'),
      uglify(),
      rename('/all.min.js'),
      gulp.dest('./dist/js/')
    ],
    cb
  );
});

// Task to assemble a project
gulp.task('build', gulpSequence('clean-dist', 'concat-js', ['sass', 'image-min', 'compress-js']));

// Task to start in browser
gulp.task('dev', function() {
  browserSync.init({
    server: {
      baseDir: '.'
    }
  });
  gulp.watch('./src/scss/**/*.scss', ['sass']).on('change', browserSync.reload);
  gulp.watch('./src/js/**/*.js', ['concat-js', 'compress-js']).on('change', browserSync.reload);
});

